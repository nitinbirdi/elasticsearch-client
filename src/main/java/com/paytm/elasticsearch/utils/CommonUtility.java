package com.paytm.elasticsearch.utils;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

public class CommonUtility {

    public static String createJsonObject(Object obj) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }
}
