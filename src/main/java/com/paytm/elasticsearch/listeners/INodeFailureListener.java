package com.paytm.elasticsearch.listeners;

import org.elasticsearch.client.Node;

public interface INodeFailureListener {

    public void onFailure(Node node);
}
