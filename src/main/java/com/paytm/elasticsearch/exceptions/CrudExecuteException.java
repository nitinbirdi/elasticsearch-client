package com.paytm.elasticsearch.exceptions;

public class CrudExecuteException extends Exception {

    public static final String MESSAGE = "Exception while executing crud execute";

    public CrudExecuteException() {
        super(MESSAGE);
    }

    public CrudExecuteException(String message) {
        super(message);
    }

    public CrudExecuteException(String message, Throwable cause) {
        super(message, cause);
    }

    public CrudExecuteException(Throwable cause) {
        super(cause);
    }
}
