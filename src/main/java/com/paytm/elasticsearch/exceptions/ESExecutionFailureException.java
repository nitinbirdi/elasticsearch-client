package com.paytm.elasticsearch.exceptions;

public class ESExecutionFailureException extends Exception {

    public static final String MESSAGE = "Exception while executing ES query";

    public static final Integer CODE = 500;

    public int getErrorCode() {
        return CODE;
    }

    public ESExecutionFailureException() {
        super(MESSAGE);
    }

    public ESExecutionFailureException(String message) {
        super(message);
    }

    public ESExecutionFailureException(String message, Throwable cause) {
        super(message, cause);
    }

    public ESExecutionFailureException(Throwable cause) {
        super(MESSAGE, cause);
    }
}
