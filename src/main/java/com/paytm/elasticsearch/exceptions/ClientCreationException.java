package com.paytm.elasticsearch.exceptions;

public class ClientCreationException extends Exception {

    public static final String MESSAGE = "Client could not be created";

    public ClientCreationException() {
        super(MESSAGE);
    }

    public ClientCreationException(String message) {
        super(message);
    }

    public ClientCreationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClientCreationException(Throwable cause) {
        super(cause);
    }
}
