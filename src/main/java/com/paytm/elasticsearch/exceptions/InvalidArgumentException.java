package com.paytm.elasticsearch.exceptions;

public class InvalidArgumentException extends Exception {

    private final static String MESSAGE = "Invalid Argument";

    public InvalidArgumentException() {
        super(MESSAGE);
    }

    public InvalidArgumentException(String message) {
        super(message);
    }

    public InvalidArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidArgumentException(Throwable cause) {
        super(cause);
    }

    protected InvalidArgumentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
