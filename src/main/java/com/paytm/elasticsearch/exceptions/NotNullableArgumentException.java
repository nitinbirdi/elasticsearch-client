package com.paytm.elasticsearch.exceptions;

public class NotNullableArgumentException extends Exception {

    public static final String MESSAGE = "Argument can not be null";

    public NotNullableArgumentException() {
        super(MESSAGE);
    }

    public NotNullableArgumentException(String message) {
        super(message);
    }

    public NotNullableArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotNullableArgumentException(Throwable cause) {
        super(cause);
    }
}
