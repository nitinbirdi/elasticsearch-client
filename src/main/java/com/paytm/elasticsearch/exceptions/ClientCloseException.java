package com.paytm.elasticsearch.exceptions;

public class ClientCloseException extends Exception {

    public static final String MESSAGE = "Exception while calling client.close()";

    public ClientCloseException() {
        super(MESSAGE);
    }

    public ClientCloseException(String message) {
        super(message);
    }

    public ClientCloseException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClientCloseException(Throwable cause) {
        super(cause);
    }
}
