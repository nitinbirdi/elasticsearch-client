package com.paytm.elasticsearch.actions;

import com.paytm.elasticsearch.exceptions.NotNullableArgumentException;
import com.paytm.elasticsearch.models.IESDocument;
import com.paytm.elasticsearch.models.IndexMeta;
import com.paytm.elasticsearch.utils.CommonUtility;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.admin.cluster.repositories.delete.DeleteRepositoryAction;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.common.xcontent.XContentType;

public class DeleteAction extends AbstractCRUD {

    public DocWriteRequest<?> getActionRequest(IndexMeta meta, IESDocument obj) throws NotNullableArgumentException {
        if (null != obj.getDocumentId()) {
            throw new NotNullableArgumentException();
        }

        return new DeleteRequest(meta.getIndex()[0]).id(obj.getDocumentId()).routing(obj.getRoutingKey());
    }
}
