package com.paytm.elasticsearch.actions;

import com.paytm.elasticsearch.models.IESDocument;
import com.paytm.elasticsearch.models.IndexMeta;
import com.paytm.elasticsearch.utils.CommonUtility;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.IOException;

public class IndexAction extends AbstractCRUD {

    public DocWriteRequest<?> getActionRequest(IndexMeta meta, IESDocument obj) throws IOException {
        IndexRequest request = new IndexRequest(meta.getIndex()[0]).routing(obj.getRoutingKey()).source(XContentType.JSON , CommonUtility.createJsonObject(obj));
        if (null != obj.getDocumentId()) {
            request.id(obj.getDocumentId());
        }
        return request;
    }
}
