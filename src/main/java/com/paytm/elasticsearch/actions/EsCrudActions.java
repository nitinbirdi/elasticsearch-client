package com.paytm.elasticsearch.actions;

public enum EsCrudActions {

    INDEX, UPDATE, DELETE
}
