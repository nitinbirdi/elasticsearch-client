package com.paytm.elasticsearch.actions;

import com.paytm.elasticsearch.exceptions.InvalidArgumentException;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ActionFactory {

    private static Map<EsCrudActions, AbstractCRUD> mapObjects = new ConcurrentHashMap<>();

    public static AbstractCRUD getInstance(EsCrudActions action) throws InvalidArgumentException {

        if (!mapObjects.containsKey(action)) {
            switch (action) {
                case INDEX:
                    if (!mapObjects.containsKey(action)) {
                        mapObjects.put(action, new IndexAction());
                    }
                    break;
                case UPDATE:
                    if (!mapObjects.containsKey(action)) {
                        mapObjects.put(action, new UpdateAction());
                    }
                    break;
                case DELETE:
                    if (!mapObjects.containsKey(action)) {
                        mapObjects.put(action, new DeleteAction());
                    }
                    break;
                default:
                    throw new InvalidArgumentException(action.name());
            }
        }
        return mapObjects.get(action);
    }
}
