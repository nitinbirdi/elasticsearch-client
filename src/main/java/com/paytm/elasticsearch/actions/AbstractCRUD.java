package com.paytm.elasticsearch.actions;

import com.paytm.elasticsearch.exceptions.NotNullableArgumentException;
import com.paytm.elasticsearch.models.CrudResponse;
import com.paytm.elasticsearch.models.IESDocument;
import com.paytm.elasticsearch.models.IndexMeta;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractCRUD {

    public abstract DocWriteRequest<?> getActionRequest(IndexMeta meta, IESDocument obj) throws NotNullableArgumentException, IOException;

    public CrudResponse execute(IndexMeta meta, List<IESDocument> listObjects, RestHighLevelClient client) throws Exception {

        BulkRequest request = new BulkRequest();
        int totalItems = 0;
        Map<String, String> failedIds = null;

        for (IESDocument obj : listObjects) {
            DocWriteRequest<?> actionRequest = getActionRequest(meta, obj);

            if (null != obj.getRoutingKey()) {
                actionRequest.routing(obj.getRoutingKey());
            }

            request.add(actionRequest);
        }

        BulkResponse bulkResponse = client.bulk(request, RequestOptions.DEFAULT);
        
        if (bulkResponse != null && bulkResponse.hasFailures()) {

            failedIds = new HashMap<String, String>();

            for (BulkItemResponse itemResponse : bulkResponse) {
                if (itemResponse.isFailed()) {
                    failedIds.put(itemResponse.getId(), itemResponse.getFailureMessage());
                }
            }
        }
        return new CrudResponse(totalItems, failedIds);
    }
}
