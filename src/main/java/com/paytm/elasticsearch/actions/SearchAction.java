package com.paytm.elasticsearch.actions;

import com.paytm.elasticsearch.models.IndexOption;
import com.paytm.elasticsearch.models.SearchModel;
import com.paytm.elasticsearch.models.SortMeta;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.BaseAggregationBuilder;
import org.elasticsearch.search.aggregations.PipelineAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.util.logging.Logger;

public class SearchAction {

    public static SearchResponse search(RestHighLevelClient client, SearchModel searchMeta, QueryBuilder queryBuilder, BaseAggregationBuilder... aggregations) throws IOException {
        SearchRequest request = new SearchRequest(searchMeta.getIndex());
        request.routing(searchMeta.getRoutingKey());
        if(searchMeta.getIndexOption()!=null) {
            IndexOption indexOption = searchMeta.getIndexOption();
            request.indicesOptions(request.indicesOptions().fromOptions(indexOption.isIgnoreUnavailable(), indexOption.isAllowNoIndices(), indexOption.isExpandToOpenIndices(), indexOption.isExpandToClosedIndices()));
        }

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(queryBuilder);

        if(aggregations!=null){
            for (BaseAggregationBuilder aggregation : aggregations) {
                if (aggregation instanceof AggregationBuilder) {
                    searchSourceBuilder.aggregation((AggregationBuilder) aggregation);
                } else {
                    searchSourceBuilder.aggregation((PipelineAggregationBuilder) aggregation);
                }
            }
        }


        searchSourceBuilder.from(searchMeta.getOffset()).size(searchMeta.getSize());

        SortMeta sortMeta = searchMeta.getSortmeta();

        if (null != sortMeta) {
            searchSourceBuilder.sort(sortMeta.getField(), sortMeta.getSortOrder());
        }

        return client.search(request.source(searchSourceBuilder), RequestOptions.DEFAULT);
    }

}
