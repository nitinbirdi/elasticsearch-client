package com.paytm.elasticsearch.actions;

import com.paytm.elasticsearch.exceptions.NotNullableArgumentException;
import com.paytm.elasticsearch.models.IESDocument;
import com.paytm.elasticsearch.models.IndexMeta;
import com.paytm.elasticsearch.utils.CommonUtility;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.IOException;

/**
 * TO Do:
 * Add script support
 *
 */
public class UpdateAction extends AbstractCRUD {

    public DocWriteRequest<?> getActionRequest(IndexMeta meta, IESDocument obj) throws NotNullableArgumentException, IOException {
        if (null != obj.getDocumentId()) {
            throw new NotNullableArgumentException();
        }

        return new UpdateRequest().index(meta.getIndex()[0]).id(obj.getDocumentId()).routing(obj.getRoutingKey()).doc(XContentType.JSON , CommonUtility.createJsonObject(obj));
    }
}
