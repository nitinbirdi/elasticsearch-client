package com.paytm.elasticsearch.client;

public enum TimeOuts {
    CONNECT_TIMEOUT, SOCKET_TIMEOUT, RETRY_TIMEOUT
}
