package com.paytm.elasticsearch.client;

import com.paytm.elasticsearch.actions.*;
import com.paytm.elasticsearch.exceptions.ClientCloseException;
import com.paytm.elasticsearch.exceptions.ClientCreationException;
import com.paytm.elasticsearch.exceptions.CrudExecuteException;
import com.paytm.elasticsearch.exceptions.InvalidArgumentException;
import com.paytm.elasticsearch.listeners.INodeFailureListener;
import com.paytm.elasticsearch.models.CrudResponse;
import com.paytm.elasticsearch.models.IESDocument;
import com.paytm.elasticsearch.models.IndexMeta;
import com.paytm.elasticsearch.models.SearchModel;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Node;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.logging.ESLoggerFactory;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.aggregations.BaseAggregationBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author nitinbirdi
 */
public final class ElasticClient {

    private static final Logger LOG = ESLoggerFactory.getLogger(ElasticClient.class);

    private final String clusterDns;
    private RestHighLevelClient restHighLevelClient;
    private final Map<TimeOuts, Integer> timeOutMap;
    private final INodeFailureListener failureListener;

    private ElasticClient(String clusterDns, Map<TimeOuts, Integer> timeOutMap, INodeFailureListener failureListener) throws ClientCreationException {
        this.clusterDns = clusterDns;
        this.timeOutMap = timeOutMap;
        this.failureListener = failureListener;
        buildClient();
    }

    private void registerFailureListener(RestClientBuilder builder) {
        if (null == failureListener) {
            return;
        }
        builder.setFailureListener(new RestClient.FailureListener() {
            @Override
            public void onFailure(Node node) {
                failureListener.onFailure(node);
            }
        });
    }

    public void buildClient() throws ClientCreationException {
        try {

            String[] clusterDnsArray = clusterDns.split(",");
            HttpHost[] hosts = new HttpHost[clusterDnsArray.length];

            int i = 0;

            for (String dns :  clusterDnsArray) {
                if (dns.contains(":")) {
                    String[] ipPort = dns.split(":");
                    hosts[i++] = new HttpHost(ipPort[0], Integer.parseInt(ipPort[1]));
                } else {
                    hosts[i++] = new HttpHost(dns);
                }
            }

            RestClientBuilder builder = RestClient.builder(hosts)
                    .setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback() {
                        public RequestConfig.Builder customizeRequestConfig(RequestConfig.Builder requestConfigBuilder) {
                            return requestConfigBuilder.setConnectTimeout(timeOutMap.get(TimeOuts.CONNECT_TIMEOUT))
                                    .setSocketTimeout(timeOutMap.get(TimeOuts.SOCKET_TIMEOUT));
                        }
                    })
                    .setMaxRetryTimeoutMillis(timeOutMap.get(TimeOuts.RETRY_TIMEOUT));

            registerFailureListener(builder);

            restHighLevelClient = new RestHighLevelClient(builder);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            throw new ClientCreationException(e);
        }
    }

    public synchronized void close() throws ClientCloseException {
        try {
            restHighLevelClient.close();
        } catch (IOException e) {
            LOG.error(e.getMessage());
            throw new ClientCloseException(e);
        }
    }

    private AbstractCRUD getActionObject(EsCrudActions action) throws InvalidArgumentException {
        return ActionFactory.getInstance(action);
    }

    private CrudResponse execute(AbstractCRUD actionObject, List<IESDocument> listObjects, RestHighLevelClient client, IndexMeta meta) throws CrudExecuteException, ClientCreationException {
        CrudResponse response = null;
        try {
            response = actionObject.execute(meta, listObjects, client);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            throw new CrudExecuteException(e);
        }
        return response;
    }

    public CrudResponse index(IndexMeta meta, List<IESDocument> listObjects) throws InvalidArgumentException, CrudExecuteException, ClientCreationException {
        IndexAction action = (IndexAction) getActionObject(EsCrudActions.INDEX);
        return execute(action, listObjects, restHighLevelClient, meta);
    }

    public CrudResponse update(IndexMeta meta, List<IESDocument> listObjects) throws InvalidArgumentException, CrudExecuteException, ClientCreationException {
        UpdateAction action = (UpdateAction) getActionObject(EsCrudActions.UPDATE);
        return execute(action, listObjects, restHighLevelClient, meta);
    }

    public CrudResponse delete(IndexMeta meta, List<IESDocument> listObjects) throws InvalidArgumentException, CrudExecuteException, ClientCreationException {
        DeleteAction action = (DeleteAction) getActionObject(EsCrudActions.DELETE);
        return execute(action, listObjects, restHighLevelClient, meta);
    }

    public SearchResponse search(SearchModel searchMeta, QueryBuilder queryBuilder, BaseAggregationBuilder... aggregations) throws IOException, ClientCreationException {
        return SearchAction.search(restHighLevelClient, searchMeta, queryBuilder, aggregations);
    }

    public static class ElasticClientBuilder {

        private String nodeDns;
        private Map<TimeOuts, Integer> timeOutMap;
        private INodeFailureListener failureListener;

        public ElasticClientBuilder(String dns) throws Exception {
            this.nodeDns = dns;
            timeOutMap = new ConcurrentHashMap<TimeOuts, Integer>();
            timeOutMap.put(TimeOuts.CONNECT_TIMEOUT, RestClientBuilder.DEFAULT_CONNECT_TIMEOUT_MILLIS);
            timeOutMap.put(TimeOuts.SOCKET_TIMEOUT, RestClientBuilder.DEFAULT_SOCKET_TIMEOUT_MILLIS);
            timeOutMap.put(TimeOuts.RETRY_TIMEOUT, RestClientBuilder.DEFAULT_MAX_RETRY_TIMEOUT_MILLIS);
        }

        public void withTimeOuts(TimeOuts entity, Integer timeInMillis) {
            timeOutMap.put(entity, timeInMillis);
        }

        public void withMaxConnections(Integer maxConnections) {
            /*
            To Do
             */
        }

        public void registerFailureListener(INodeFailureListener failureListener) {
            this.failureListener = failureListener;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ElasticClientBuilder)) return false;

            ElasticClientBuilder that = (ElasticClientBuilder) o;

            if (!nodeDns.equals(that.nodeDns)) return false;
            if (timeOutMap != null ? !timeOutMap.equals(that.timeOutMap) : that.timeOutMap != null) return false;
            return failureListener != null ? failureListener.equals(that.failureListener) : that.failureListener == null;
        }

        public ElasticClient build() throws ClientCreationException {
            return new ElasticClient(nodeDns, timeOutMap, failureListener);
        }

    }
}
