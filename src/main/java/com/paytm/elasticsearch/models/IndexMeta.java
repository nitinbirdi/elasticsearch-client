package com.paytm.elasticsearch.models;

public class IndexMeta {

    protected String[] index;

    public IndexMeta(String[] index) {
        this.index = index;
    }

    public String[] getIndex() {
        return index;
    }

    public void setIndex(String[] index) {
        this.index = index;
    }

}
