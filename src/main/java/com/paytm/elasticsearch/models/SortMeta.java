package com.paytm.elasticsearch.models;

import org.elasticsearch.search.sort.SortOrder;

public class SortMeta {

    private String field;
    private SortOrder sortOrder;

    public SortMeta(String field) {
        this.field = field;
        this.sortOrder = SortOrder.DESC;
    }

    public SortMeta(String field, SortOrder sortOrder) {
        this.field = field;
        this.sortOrder = sortOrder;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }
}
