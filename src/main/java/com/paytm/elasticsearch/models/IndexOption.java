package com.paytm.elasticsearch.models;

public class IndexOption {

  private boolean ignoreUnavailable;

  private boolean allowNoIndices;

  private boolean expandToOpenIndices;

  private boolean expandToClosedIndices;

  public IndexOption() {
  }

  public IndexOption(boolean ignoreUnavailable, boolean allowNoIndices, boolean expandToOpenIndices, boolean expandToClosedIndices) {
    this.ignoreUnavailable = ignoreUnavailable;
    this.allowNoIndices = allowNoIndices;
    this.expandToOpenIndices = expandToOpenIndices;
    this.expandToClosedIndices = expandToClosedIndices;
  }

  public boolean isIgnoreUnavailable() {
    return ignoreUnavailable;
  }

  public boolean isAllowNoIndices() {
    return allowNoIndices;
  }

  public boolean isExpandToOpenIndices() {
    return expandToOpenIndices;
  }

  public boolean isExpandToClosedIndices() {
    return expandToClosedIndices;
  }
}
