package com.paytm.elasticsearch.models;

public interface IESDocument {

    public String getDocumentId();
    public String getRoutingKey();
}
