package com.paytm.elasticsearch.models;

public class SearchModel extends IndexMeta {

    private int size = Constants.SIZE;
    private int offset = Constants.OFFSET;
    private String[] routingKey;
    private SortMeta sortmeta;
    private IndexOption indexOption;

    public SearchModel(String[] index, String[] routingKey, int size, int offset) {
        super(index);
        this.size = size;
        this.offset = offset;
        this.routingKey = routingKey;
    }


    public SearchModel(String[] index, String[] routingKey, int size, int offset, SortMeta sortMeta) {
        super(index);
        this.size = size;
        this.offset = offset;
        this.routingKey = routingKey;
        this.sortmeta = sortMeta;
    }

    public SearchModel(String[] index, String[] routingKey, IndexOption indexOption) {
        super(index);
        this.routingKey = routingKey;
        this.indexOption = indexOption;
    }

    public SearchModel(String[] index, String[] routingKey) {
        super(index);
        this.routingKey = routingKey;
    }

    public SearchModel(String[] index, int size, String[] routingKey, IndexOption indexOption) {
        super(index);
        this.size = size;
        this.routingKey = routingKey;
        this.indexOption = indexOption;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String[] getRoutingKey() {
        return routingKey;
    }

    public SortMeta getSortmeta() {
        return sortmeta;
    }

    public void setSortmeta(SortMeta sortmeta) {
        this.sortmeta = sortmeta;
    }

    public void setRoutingKey(String[] routingKey) {
        this.routingKey = routingKey;
    }

    public IndexOption getIndexOption() {
        return indexOption;
    }
}
