package com.paytm.elasticsearch.models;

import java.util.Map;

public class CrudResponse {

    Integer totalItems;
    Map<String, String> failedIds;

    public CrudResponse(Integer totalItems, Map<String, String> failedIds) {
        this.totalItems = totalItems;
        this.failedIds = failedIds;
    }

    public Integer getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
    }

    public Map<String, String> getFailedIds() {
        return failedIds;
    }

    public void setFailedIds(Map<String, String> failedIds) {
        this.failedIds = failedIds;
    }
}
